# Data Engineer Test Case Dec - Jan 2022

Buatlah masing-masing satu *Gitlab Snippet* untuk menyimpan jawaban untuk setiap pertanyaan di bawah.
Pastikan *Snippet* berisi code yang lengkap dan **dibagikan secara publik**.

Cara membuat Snippet: https://docs.gitlab.com/ee/user/snippets.html

Setelah menyimpan *Snippet* dengan *visibility level: public*, salin *share link* dari *snippet* tersebut ke kolom isian di Google Form.

Contoh Snippet Share link: https://gitlab.com/-/snippets/2222088

Selamat mengerjakan 👍

---


## **Soal Pertama: SQL Test**

Contoh kasus:

Tim *Data Engineer* menerima permintaan dari tim *Data Analyst* untuk menyediakan sebuah data agregat. Data ini akan digunakan untuk penelitian lebih lanjut tentang korelasi antara peningkatan aktivitas masyarakat dengan jumlah kasus terkonfirmasi Covid-19.

Salah satu indikator tingginya aktivitas masyarakat dilihat dari rata-rata kepadatan lalu-lintas kendaraan di sebuah wilayah. Dalam contoh kasus ini, angka kepadatan disederhanakan dalam cakupan wilayah Kabupaten/Kota.

### **Sumber Data:**
### **1. Tabel Data Kasus Konfirmasi Covid-19 Bulan Juli 2020**

![Tabel Data Kasus Konfirmasi Covid-19 Bulan Juli 2020](/screenshots/table_1.png "Tabel Data Kasus Konfirmasi Covid-19 Bulan Juli 2020")

Gunakan sql script berikut untuk membuat tabel di PostgreSQL:

[Download Insert Query Tabel Data Kasus Terkonfirmasi](/Dataset/kasus_konfirmasi_juli.sql "Download Insert Query Tabel Data Kasus Terkonfirmasi")


### **Penjelasan Tabel**

| Nama Kolom                       | Tipe Data | Deskripsi                                                  |
| -------------------------------- | --------- | ---------------------------------------------------------- |
| tanggal                          | date      | Tanggal YYYY-MM-DD (Agregat)                               |
| nama\_kab\_kota                  | varchar   | Nama Wilayah (Kab/Kota)                                    |
| kode\_kab\_kota                  | varchar   | Kode Wilayah (Kab/Kota)                                    |
| konfirmasi\_total                | int       | Jumlah Kasus Terkonfirmasi pada tanggal ini                |
| konfirmasi\_sembuh               | int       | Jumlah Kasus yang dinyatakan sembuh (positif sembuh)       |
| konfirmasi\_meninggal            | int       | Jumlah Kasus yang dinyatakan meninggal (positif meninggal) |
| konfirmasi\_aktif                | int       | Jumlah Kasus positif yg masih aktif hari ini               |
| konfirmasi\_total\_daily\_growth | int       | Jumlah percepatan kasus (total)                            |
| id                               | int       | id row                                                     |


### **2. Tabel Data Agregat Kepadatan Jalan**

![Tabel Data Agregat Kepadatan Jalan](/screenshots/table_2.png "Tabel Data Agregat Kepadatan Jalan")

Gunakan sql script berikut untuk membuat tabel di PostgreSQL:

[Download Insert Query Tabel Data Agregat Kepadatan Jalan](/Dataset/aggregate_jams_cities_june_july_2020.sql "Download Insert Query Tabel Data Agregat Kepadatan Jalan")

### **Penjelasan Tabel**
                                                                
| Nama Kolom                  | Tipe Data        | Deskripsi                                                                                                                                  |
| --------------------------- | ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| date                        | date             | Tanggal YYYY-MM-DD (Agregat)                                                                                                               |
| kemendagri\_kabupaten\_kode | varchar          | Kode Wilayah (Kab/Kota)                                                                                                                    |
| kemendagri\_kabupaten\_nama | varchar          | Nama Wilayah (Kab/Kota)                                                                                                                    |
| med\_level                  | double precision | Median Tingkat Level Kemacetan (0=freeflow,5=blocked)                                                                                      |
| avg\_length                 | double precision | Rata-rata panjang kemacetan                                                                                                                |
| avg\_delay                  | double precision | Rata-rata waktu perjalanan tambahan akibat kemacetan (dihitung dari waktu aktual perjalanan - perkiraan waktu tempuh apabila jalan lancar) |
| avg\_speed\_kmh             | double precision | Rata-rata kecepatan kendaraan                                                                                                              |
| med\_total\_records         | double precision | Median jumlah laporan kemacetan                                                                                                            |

### **Task:**
Buatlah sebuah sql query gabungan dari dua dataset diatas:

Tabel Data Kasus Konfirmasi Covid-19 (1) ditambah kolom-kolom dari Tabel Data Agregat Kepadatan Jalan (2):
- med_level
- avg_delay
- avg_speed_kmh
- total_records

**Ketentuan khusus:**

1. Data kepadatan jalan diambil dari 5 hari sebelumnya**

Contoh:
Pada tanggal 2020-07-01 di Kab. Bogor (3201) ada 249 total kasus terkonfirmasi. Namun data kepadatan jalan yang disandingkan pada baris ini diambil dari tanggal 2020-06-25 di tabel data agregat kepadatan jalan

![Data kepadatan jalan 5 hari kebelakang](/screenshots/5_days_ago.png "Data kepadatan jalan 5 hari kebelakang")

2. Data kepadatan lalu lintas dapat diisi dengan value NULL untuk beberapa kabupaten/kota yang belum memiliki data kepadatan lalu-lintas. 

**Contoh hasil query yang diinginkan:**

Screenshot:

![Query Output Sample](/screenshots/output_sample.png "Query Output Sample")

[Sample Output CSV](/Dataset/output_sql.csv "Sample output CSV")

---

## **Soal Kedua: Python Test**

Pilih salah satu dari media online Jawa Barat berikut:

- https://www.pikiran-rakyat.com/jawa-barat
- https://www.kompas.com/tag/jawa+barat
- https://jabar.antaranews.com/
- https://jabar.tribunnews.com/
- https://mediaindonesia.com/tag/jabar
- https://news.detik.com/jawabarat
- https://bandung.bisnis.com/

Lalu tulislah sebuah script scraping menggunakan python untuk mengambil 10 berita terkini ditambah sampel 5 kata paling umum (most common words) dari isi beritanya.

Kata-kata paling umum yang diambil yang memiliki makna dan **tidak termasuk** dari contoh kata-kata berikut:

```
- dan
- dari
- di
- dengan
- ke
- oleh
- pada
- sejak
- sampai
- seperti
- untuk
- buat
- bagi
- akan
- antara
- demi
- hingga
- kecuali
- tentang
- seperti
- serta
- tanpa
- kepada
- daripada
- oleh karena itu
- antara
- dengan
- sejak
- sampai
- bersama
- beserta
- menuju
- menurut
- sekitar
- selama
- seluruh
- bagaikan
- terhadap
- melalui
- mengenai
```


Hasil output berupa json dengan contoh seperti berikut

```json
[{
		"scrape_time": "timestamp saat scraping",
		"story_source": "nama portal berita",
		"story_release_date": "timestamp atau tanggal rilis berita tertera",
		"story_title": "Judul Berita",
		"story_url": "URL Berita",
		"most_common_words": ["lima", "kata", "paling", "populer"]
	},
	{
		"scrape_time": "2021-12-13 14:03:02",
		"story_source": "pikiran_rakyat_jabar",
		"story_release_date": "2021-12-13 10:29:00",
		"story_title": "Lebih dari 8.000 Pelanggan di Jabar Mengikuti Program Electrifying Agriculture",
		"story_url": "https://www.pikiran-rakyat.com/jawa-barat/pr-013221117/lebih-dari-8000-pelanggan-di-jabar-mengikuti-program-electrifying-agriculture",
		"most_common_words": ["petani", "listrik", "pelanggan", "sektor", "pertanian"]
	},
    ...
]
```